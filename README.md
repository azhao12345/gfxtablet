IvanDSM's GfxTablet fork
==================

This version of GfxTablet is going to be incompatible with rfc2822's GfxTablet. Its goal is to expand the software capabilities by giving the user more advanced options.

Because of my procrastination, though, i don't think this will be done too quickly.


License
-------

The original and modded versions of GfxTablet are licensed under The MIT License.

Author: Richard Hirner

Powered by [bitfire web engineering](http://www.bitfire.at) / [gimpusers.com](http://www.gimpusers.com)


Features
--------

* Pressure sensitivity supported
* Size of canvas will be detected and sent to the client
* Option for ignoring events that are not triggered by a stylus pen:
  so you can lay your hand on the tablet and draw with the pen.
* [TO BE IMPLEMENTED] The ability to choose if the computer shall receive both motion and clicks or only motion data.
* [TO BE IMPLEMENTED] Optional reduction of delay by using a 1-bit signal [0 -1] for pressure instead of standard full range [0 - 65535].
* 


Requirements
------------

* App: Any device with Android 4.0+ and touch screen
* Driver: Linux with uinput kernel module (included in modern versions of Fedora, Ubuntu etc.)

If you use Xorg (you probably do):

* Xorg-evdev module loaded and configured
  Probably on by default, but if it doesn't work, you may need to activate the
  module: see https://github.com/rfc2822/GfxTablet/issues/7#issuecomment-13338216 (on the original GfxTablet github page)


Installation
============

Github repository: http://github.com/IvanDSM/GfxTablet


Part 1: uinput driver
---------------------

Compile it yourself (don't be afraid, it's only one file)

1. Clone the repository:
   `git clone git://github.com/IvanDSM/GfxTablet.git`
2. Install gcc, make and linux kernel header includes (`kernel-headers` on Fedora)
3. `cd driver-uinput; make`

Then, run the binary. The driver runs in user-mode, so it doesn't need any special privileges.
However, it needs access to `/dev/uinput`. If your distribution doesn't create a group for
uinput access, you'll need to do it yourself or just run the driver as root:

`sudo ./networktablet`

Then you should see a status message saying the driver is ready. If you do `xinput list` in a separate
terminal, should show a "Network Tablet" device.

You can start and stop (Ctrl+C) the Network Tablet at any time, but please be aware that applications
which use the device may be confused by that and could crash.

`networktablet` will display a status line for every touch/motion event it receives.


Part 2: App
-----------

Compile the app from the source code in the Git repository
After installing, enter your host IP in the Settings / Host name and it should be ready.


Part 3: Use it
--------------

Now you can use your tablet as an input device in every Linux application (including X.org
applications). For instance, when networktablet is running, GIMP should have a "Network Tablet"
entry in "Edit / Input Devices". Set its mode to "Screen" and it's ready to use.


Support
=======

For bug reports, please use the [Github issues page](https://github.com/IvanDSM/GfxTablet/issues)
or just fork the repository, fix the bug and send a merge request.
